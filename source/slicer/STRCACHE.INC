type
    PStringCacheItem = ^TStringCacheItem;
    TStringCacheItem = record
        Uniq : LongInt;
        Str : PString;
        Next : PStringCacheItem;
    end;
    PStringCache = ^TStringCache;
    TStringCache = object
        FirstStr, LastStr : PStringCacheItem;
        Count, Max  : LongInt;
        constructor Create(AMax : LongInt);
        destructor Destroy; virtual;
        procedure Clear; virtual;
        procedure Reduce(ACount : LongInt); virtual;
        procedure Put(AUniq : LongInt; AString : String); virtual;
        function Find(AString : String) : LongInt; virtual;
        function Get(AUniq : LongInt) : String; virtual;
    end;

constructor TStringCache.Create(AMax : LongInt);
begin
    FirstStr := nil;
    LastStr := nil;
    Count := 0;
    Max := AMax;
end;

destructor TStringCache.Destroy;
begin
    Clear;
end;

procedure TStringCache.Clear;
begin
   Reduce(Count);
end;

procedure TStringCache.Reduce(ACount : LongInt);
var
    P : PStringCacheItem;
begin
    While (ACount > 0) and Assigned(FirstStr) do begin
        if LastStr = FirstStr then
            LastStr := nil;
        P := FirstStr^.Next;
        FreeStr(FirstStr^.Str);
        Dispose(FirstStr);
        FirstStr := P;
        Dec(Count);
        Dec(ACount);
    end
end;

procedure TStringCache.Put(AUniq : LongInt; AString : String);
var
    P : PStringCacheItem;
begin
    CheckMemory(Sizeof(TStringCacheItem));
    P := New(PStringCacheItem);
    P^.Next := nil;
    P^.Str  := StrPtr(AString);
    P^.Uniq := AUniq;
    if Assigned(LastStr) then begin
        LastStr^.Next := P;
        LastStr := P;
    end else begin
        FirstStr := P;
        LastStr := FirstStr;
    end;
    Inc(Count);
    if Count > Max then
        Reduce(Count - Max);
end;

function TStringCache.Find(AString : String) : LongInt;
var
    P : PStringCacheItem;
begin
    P := FirstStr;
    Find := -1;
    While Assigned(P) do begin
        if AString = PtrStr(P^.Str) then begin
            Find := P^.Uniq;
            P := LastStr;
        end;
        P := P^.Next;
    end;
end;

function TStringCache.Get(AUniq : LongInt) : String;
var
    P : PStringCacheItem;
begin
    P := FirstStr;
    Get := '';
    While Assigned(P) do begin
        if AUniq = P^.Uniq then begin
            Get := PtrStr(P^.Str);
            P := LastStr;
        end;
        P := P^.Next;
    end;
end;

