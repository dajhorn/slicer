@echo off

deltree /y test.* >nul

slicer /v /f test /s 50k /cvm demo\header.txt
if errorlevel 1 goto Failed

slicer /v /f test /g src /m demo\sources.es /r /L es
if errorlevel 1 goto Failed
slicer /v /p gz /f test /g src /m demo\sources.txt *.pas *.bat /dr
if errorlevel 1 goto Failed
slicer /v /p gz /f test /g src /m demo\sources.txt *.pas *.inc *.bat /dr
if errorlevel 1 goto Failed

slicer /p gz /f test /g bin,exe,default /r /L es /m demo\execute.es
if errorlevel 1 goto Failed
slicer /p gz /f test /g bin,exe,default *.exe /r /m demo\execute.txt
if errorlevel 1 goto Failed

slicer /p gz /f test /g nls,exe,default /m demo\nls.es /r /L es
if errorlevel 1 goto Failed
slicer /p gz /f test /g nls,exe,default /m demo\nls.txt /r
if errorlevel 1 goto Failed
slicer /p gz /f test /g src,nls,exe,default nls\*.* /dr
if errorlevel 1 goto Failed

slicer /p gz /f test /g qcrt,src /r /L es /M demo\qcrt.es
if errorlevel 1 goto Failed
slicer /p gz /f test /g qcrt,src /r /M demo\qcrt.txt
if errorlevel 1 goto Failed
slicer /p gz /f test /g qcrt,src QCrt /dre *.tpu /e *.obj /m demo\stars.txt
if errorlevel 1 goto Failed

slicer /p gz /f test /g tpu,qcrt,bin QCrt /drE demo\EXCLUDE.LST /e *.obj
if errorlevel 1 goto Failed
slicer /p gz /f test /g obj,qcrt,bin QCrt /drE demo\EXCLUDE.LST /e *.tpu
if errorlevel 1 goto Failed

slicer /p gz /f test /g * /m demo\thanks.es /rq /L es
if errorlevel 1 goto Failed
slicer /p gz /f test /g * /m demo\thanks.txt /rq
if errorlevel 1 goto Failed

goto Done

slicer /Rvf test

slicer /txf test /g * *.pas
slicer /txf test /g src *.bat
slicer /txf test /g bin

goto Done
:Failed
vecho /f Red Failed. /fGrey
verrlvl 1

:Done
